json.extract! entry, :id, :name, :time_spent, :created_at, :updated_at
json.url entry_url(entry, format: :json)
