json.extract! animal, :id, :name, :weight, :created_at, :updated_at
json.url animal_url(animal, format: :json)
