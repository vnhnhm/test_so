json.extract! full_calendar_engine, :id, :name, :site_id, :created_at, :updated_at
json.url full_calendar_engine_url(full_calendar_engine, format: :json)
