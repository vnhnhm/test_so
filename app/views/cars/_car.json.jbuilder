json.extract! car, :id, :model, :kilometers, :year, :color, :price, :created_at, :updated_at
json.url car_url(car, format: :json)
