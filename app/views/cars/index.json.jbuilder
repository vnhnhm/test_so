json.cars do |json|
  json.array! @cars, partial: 'cars/car', as: :car
end
