class FullCalendarEnginesController < ApplicationController
  before_action :set_full_calendar_engine, only: [:show, :edit, :update, :destroy]

  # GET /full_calendar_engines
  # GET /full_calendar_engines.json
  def index
    @full_calendar_engines = FullCalendarEngine.all
  end

  # GET /full_calendar_engines/1
  # GET /full_calendar_engines/1.json
  def show
  end

  # GET /full_calendar_engines/new
  def new
    @full_calendar_engine = FullCalendarEngine.new
  end

  # GET /full_calendar_engines/1/edit
  def edit
  end

  # POST /full_calendar_engines
  # POST /full_calendar_engines.json
  def create
    @full_calendar_engine = FullCalendarEngine.new(full_calendar_engine_params)

    respond_to do |format|
      if @full_calendar_engine.save
        format.html { redirect_to @full_calendar_engine, notice: 'Full calendar engine was successfully created.' }
        format.json { render :show, status: :created, location: @full_calendar_engine }
      else
        format.html { render :new }
        format.json { render json: @full_calendar_engine.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /full_calendar_engines/1
  # PATCH/PUT /full_calendar_engines/1.json
  def update
    respond_to do |format|
      if @full_calendar_engine.update(full_calendar_engine_params)
        format.html { redirect_to @full_calendar_engine, notice: 'Full calendar engine was successfully updated.' }
        format.json { render :show, status: :ok, location: @full_calendar_engine }
      else
        format.html { render :edit }
        format.json { render json: @full_calendar_engine.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /full_calendar_engines/1
  # DELETE /full_calendar_engines/1.json
  def destroy
    @full_calendar_engine.destroy
    respond_to do |format|
      format.html { redirect_to full_calendar_engines_url, notice: 'Full calendar engine was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_full_calendar_engine
      @full_calendar_engine = FullCalendarEngine.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def full_calendar_engine_params
      params.require(:full_calendar_engine).permit(:name, :site_id)
    end
end
