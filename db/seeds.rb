# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

Car.delete_all

require 'json'
file = File.read("#{Dir.pwd}/db/cars.json")
json_data = JSON.parse(file)['data']

json_data.each do |key, _value|
  Car.create(
    id: key['id'],
    model: key['model'],
    kilometers: key['kilometers'],
    year: key['year'],
    color: key['color'],
    price: key['price']
  )
end

Entry.delete_all

50.times do |index|
  Entry.create!(name: "name-#{index}", time_spent: index * rand(1..5000))
end

Fruit.delete_all

5000.times do |index|
  Fruit.create!(name: "fruit_number-#{index}")
end

# @new_cars.each { |x| x.save }
