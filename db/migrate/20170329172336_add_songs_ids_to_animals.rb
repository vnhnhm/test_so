class AddSongsIdsToAnimals < ActiveRecord::Migration[5.0]
  def change
    add_column :animals, :songs_ids, :integer, array: true, default: []
  end
end
