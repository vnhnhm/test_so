class CreateFullCalendarEngines < ActiveRecord::Migration[5.0]
  def change
    create_table :full_calendar_engines do |t|
      t.string :name
      t.references :site, foreign_key: true

      t.timestamps
    end
  end
end
