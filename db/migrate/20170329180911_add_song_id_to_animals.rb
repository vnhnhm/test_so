class AddSongIdToAnimals < ActiveRecord::Migration[5.0]
  def change
    add_column :animals, :song_id, :string
  end
end
