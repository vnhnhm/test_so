def fibonacci( n )
  n.times.each_with_object([0,1]) { |num, obj| obj << obj[-2] + obj[-1] }
rescue => e
  puts e
end

puts fibonacci(50_00_000)
