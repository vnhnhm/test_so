require 'test_helper'

class FullCalendarEnginesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @full_calendar_engine = full_calendar_engines(:one)
  end

  test "should get index" do
    get full_calendar_engines_url
    assert_response :success
  end

  test "should get new" do
    get new_full_calendar_engine_url
    assert_response :success
  end

  test "should create full_calendar_engine" do
    assert_difference('FullCalendarEngine.count') do
      post full_calendar_engines_url, params: { full_calendar_engine: { name: @full_calendar_engine.name, site_id: @full_calendar_engine.site_id } }
    end

    assert_redirected_to full_calendar_engine_url(FullCalendarEngine.last)
  end

  test "should show full_calendar_engine" do
    get full_calendar_engine_url(@full_calendar_engine)
    assert_response :success
  end

  test "should get edit" do
    get edit_full_calendar_engine_url(@full_calendar_engine)
    assert_response :success
  end

  test "should update full_calendar_engine" do
    patch full_calendar_engine_url(@full_calendar_engine), params: { full_calendar_engine: { name: @full_calendar_engine.name, site_id: @full_calendar_engine.site_id } }
    assert_redirected_to full_calendar_engine_url(@full_calendar_engine)
  end

  test "should destroy full_calendar_engine" do
    assert_difference('FullCalendarEngine.count', -1) do
      delete full_calendar_engine_url(@full_calendar_engine)
    end

    assert_redirected_to full_calendar_engines_url
  end
end
