Rails.application.routes.draw do

  # devise_for :users
  # resources :posts
  resources :fruits
  # resources :animals
  # resources :books
  # resources :news

  get '/all_values', to: 'fruits#all_values'

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
